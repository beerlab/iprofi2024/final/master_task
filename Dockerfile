from ros:noetic
EXPOSE 11311
SHELL ["/bin/bash", "-c"]
ENV DEBIAN_FRONTEND noninteractive

# Install ROS extra packages
RUN apt-get update &&\ 
    apt-get install -y python3-rosdep \
        python3-rosinstall \
        python3-rosinstall-generator \
        python3-wstool \
        python3-catkin-tools \
        python3-lxml \
        python3-pip \
        build-essential \
        git wget\
    && rm -rf /var/lib/apt/lists/* && apt autoremove && apt clean
RUN apt-get update \
    && apt-get install -y ros-${ROS_DISTRO}-ros-control \ 
        ros-${ROS_DISTRO}-joint-state-controller \
        ros-${ROS_DISTRO}-velocity-controllers \
        ros-${ROS_DISTRO}-position-controllers \ 
        ros-${ROS_DISTRO}-robot-state-publisher \
        ros-${ROS_DISTRO}-teleop-twist-keyboard \
    && rm -rf /var/lib/apt/lists/* && apt autoremove && apt clean

RUN mkdir -p /ros_ws/src && cd /ros_ws \ 
    && catkin config --extend /opt/ros/${ROS_DISTRO} \
    && catkin build && echo "source /ros_ws/devel/setup.bash" >> ~/.bashrc

WORKDIR /ros_ws

# Packages for OpenCV
RUN apt-get update && apt-get install -y ffmpeg libsm6 libxext6
RUN pip3 install opencv-python
RUN apt-get install -y ros-${ROS_DISTRO}-cv-bridge ros-${ROS_DISTRO}-image-transport

COPY ./ /ros_ws/src

RUN catkin build